<?php namespace Math;


class Lexer {

	/** @var array Sequence of the tokens. */
	private $tokens = [];

	/**
	 * Split string expression into tokens.
	 *
	 * @param $expression
	 * @return array
	 * @throws InvalidArgumentException
	 */
	public function tokenize($expression)
	{
		$this->tokens = [];

		/**
		 * \d+ - any digit,
		 * chars: +, -, (, ), *, /
		 * \s+ - any whitespace,
		 *
		 * PREG_SPLIT_NO_EMPTY - only non-empty pieces will be returned,
		 * PREG_SPLIT_DELIM_CAPTURE - parenthesized expression in the delimiter pattern will be captured and returned as well
		 */
		$tokens = preg_split('((\d+|\+|-|\(|\)|\*|\/|^)|\s+)', $expression, null, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
		$tokens = array_map('trim', $tokens);

		foreach ($tokens as $i => &$token)
		{
			if ($token == Operator::SUBTRACTION
				&& $tokens[$i - 1] == Operator::ADDICTION
				&& is_numeric($tokens[$i + 1])
			)
			{
				// Fix the +-1 situations
				$tokens[$i + 1] = (-1) * (int) $tokens[$i + 1];
			}
			else if (array_key_exists($token, Operator::$operators))
			{
				$this->tokens[] = new Operator(
					$token,
					Operator::$operators[$token]['associativity']
				);
			}
			else if (is_numeric($token))
			{
				$this->tokens[] = new Token($token, Token::OPERAND);
			}
			else if ($token == '(')
			{
				$this->tokens[] = new Token($token, Token::LEFT_BRACKET);
			}
			else if ($token == ')')
			{
				$this->tokens[] = new Token($token, Token::RIGHT_BRACKET);
			}
			else
			{
				throw new InvalidArgumentException('Unknown token ' . $token);
			}
		}

		return $this->tokens;
	}

	/**
	 * Get array of tokens.
	 *
	 * @return array
	 */
	public function getTokens()
	{
		return $this->tokens;
	}
}