<?php namespace Math;


class Token {

	const OPERATOR = 1,
		OPERAND = 2,
		LEFT_BRACKET = 3,
		RIGHT_BRACKET = 4;

	protected static $types = [
		self::OPERATOR,
		self::OPERAND,
		self::LEFT_BRACKET,
		self::RIGHT_BRACKET
	];

	/**
	 * @var Token value
	 */
	private $value;

	/**
	 * @var Token type
	 */
	private $type;

	public function __construct($value, $type)
	{
		if (!in_array($type, self::$types))
			throw new InvalidArgumentException('Unknown token type: ' . $type);

		$this->value = $value;
		$this->type = $type;
	}

	/**
	 * @return int|string Token value
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * @return string Token type
	 */
	public function getType()
	{
		return $this->type;
	}
}