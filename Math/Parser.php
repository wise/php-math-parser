<?php

namespace Math;


use Math\Interfaces\IConvertor;
use Math\Interfaces\IPostfix;

class Parser {

	/** @var Lexer */
	private $lexer;

	/** @var IConvertor */
	private $convertor;

	/** @var IPostfix */
	private $postfix;

	public function __construct(
		IConvertor $convertor = null,
		IPostfix $postfix = null
	)
	{
		$this->lexer = new Lexer();
		$this->convertor = is_null($convertor) ? new ShuntingYard() : $convertor;
		$this->postfix = is_null($postfix) ? new Postfix() : $postfix;
	}

	public function evaluate($expression)
	{
		if (empty($expression))
			throw new InvalidArgumentException('Expression cannot be empty.');

		$this->convertor->convert(
			$this->lexer->tokenize($expression)
		);

		return $this->postfix->evaluate(
			$this->convertor->get()
		);
	}
}