<?php namespace Math;


class Operator extends Token {

	const ADDICTION = '+',
		SUBTRACTION = '-',
		MULTIPLICATION = '*',
		DIVISION = '/',
		POWER = '^';

	const LEFT_ASSOCIATIVE = -1;
	const NONE_ASSOCIATIVE = 0;
	const RIGHT_ASSOCIATIVE = 1;

	public static $operators = [
		'+' => ['priority' => 0, 'associativity' => self::LEFT_ASSOCIATIVE],
		'-' => ['priority' => 0, 'associativity' => self::LEFT_ASSOCIATIVE],
		'*' => ['priority' => 1, 'associativity' => self::LEFT_ASSOCIATIVE],
		'/' => ['priority' => 1, 'associativity' => self::LEFT_ASSOCIATIVE],
		'^' => ['priority' => 2, 'associativity' => self::RIGHT_ASSOCIATIVE],
	];

	/** string Operator's value */
	private $value;

	/** @var int Operator' priority */
	private $priority;

	/** @var int Operator's associativity */
	private $associativity;

	public function __construct($value, $associativity)
	{
		parent::__construct($value, Token::OPERATOR);

		$this->value = $value;
		$this->priority = self::$operators[$value];
		$this->associativity = (int) $associativity;
	}

	/**
	 * Get priority.
	 *
	 * @return int
	 */
	public function getPriority()
	{
		return $this->priority;
	}

	/**
	 * Get associativity.
	 *
	 * @return int
	 */
	public function getAssociativity()
	{
		return $this->associativity;
	}

	/**
	 * Return true if $this operator has lower priority then $operator, according to Shunting-yard algorithm.
	 *
	 * @param Operator $operator
	 * @return bool
	 */
	public function hasLowerPriority(Operator $operator)
	{
		return ($this->getAssociativity() == self::LEFT_ASSOCIATIVE && $this->getPriority() <= $operator->getPriority())
		|| ($this->getAssociativity() == self::RIGHT_ASSOCIATIVE && $this->getPriority() < $operator->getPriority());
	}
}