<?php namespace Math;

use Math\Interfaces\IPostfix;

class Postfix implements IPostfix {

	public function evaluate($tokens)
	{
		$stack = [];

		foreach ($tokens as $token)
		{
			if ($token->getType() == Token::OPERAND)
			{
				array_push($stack, $token);
			}
			else if ($token->getType() == Token::OPERATOR)
			{
				$operandOne = array_pop($stack)->getValue();
				$operandTwo = array_pop($stack)->getValue();

				$value = new Token($this->calculate(
					$operandTwo,
					$operandOne,
					$token
				), Token::OPERAND);

				array_push($stack, $value);
			}
		}

		return array_pop($stack)->getValue();
	}

	/**
	 * Do the right math.
	 *
	 * @param int $operandOne
	 * @param int $operandTwo
	 * @param Token $operator
	 * @return float|number
	 */
	protected function calculate($operandOne, $operandTwo, Token $operator)
	{
		switch ($operator->getValue())
		{
			case Operator::ADDICTION:
				return $operandOne + $operandTwo;

			case Operator::SUBTRACTION:
				return $operandOne - $operandTwo;

			case Operator::MULTIPLICATION:
				return $operandOne * $operandTwo;

			case Operator::DIVISION:
				return $operandOne / $operandTwo;

			case Operator::POWER:
				return pow($operandOne, $operandTwo);
		}
	}
}