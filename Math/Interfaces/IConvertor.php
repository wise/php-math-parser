<?php namespace Math\Interfaces;


interface IConvertor {
	public function convert($tokens);
	public function get();
}