<?php namespace Math\Interfaces;


interface IPostfix {
	public function evaluate($tokens);
}