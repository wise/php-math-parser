<?php namespace Math;


use Math\Interfaces\IConvertor;


/**
 * Class ShuntingYard
 * https://en.wikipedia.org/wiki/Shunting-yard_algorithm
 *
 * @package Math
 */
class ShuntingYard implements IConvertor {

	/** @var array */
	private $tokens;

	/** @var array output stack */
	private $output = [];

	/** @var array temporary stack */
	private $stack = [];

	/** @var array for converting operation's log */
	private $log = [];

	/** @var bool If true print log table */
	private $loggable = false;

	/**
	 * Convert tokens from the infix to the postfix (Reverse Polish notation)
	 * https://en.wikipedia.org/wiki/Reverse_Polish_notation
	 *
	 * @param $tokens array of tokens
	 */
	public function convert($tokens)
	{
		$this->tokens = $tokens;

		// While there are input tokens left, read the next token from input.
		foreach ($this->tokens as $token)
		{
			// If the token is an operator O1 ...
			if ($token instanceof Operator)
			{
				// ... while there is an operator token, O2, at the top of the operator stack, and either...
				while (end($this->stack) && end($this->stack)->getType() == Token::OPERATOR)
				{
					// ... O1 is left-associative and its priority is less than or equal to the O2 or
					// O1 is right-associative and its priority is less than O2
					// then pop O2 off the stack.
					if ($token->hasLowerPriority(end($this->stack)))
					{
						array_push($this->output, array_pop($this->stack));
						$this->log($token, 'Pop stack and add into the output.', $this->output, $this->stack);
					}
					else
					{
						break;
					}
				}

				// Push O1 to the stack.
				array_push($this->stack, $token);
				$this->log($token, 'Adding token to the stack.', $this->output, $this->stack);
			}
			else if ($token instanceof Token)
			{
				// If the token is a left parenthesis (i.e. "("), then push it onto the stack.
				if ($token->getType() == Token::LEFT_BRACKET)
				{
					array_push($this->stack, $token);
					$this->log($token, 'Adding token to the stack.', $this->output, $this->stack);
				}
				// If the token is a right parenthesis (i.e. ")")...
				elseif ($token->getType() == Token::RIGHT_BRACKET)
				{
					$leftBracket = false;

					// Until the token at the top of the stack is a left parenthesis,
					// pop operators off the stack onto the output stack.
					while (end($this->stack))
					{
						if (end($this->stack)->getType() == Token::LEFT_BRACKET)
						{
							$leftBracket = true;
							break;
						}
						else
						{
							array_push($this->output, array_pop($this->stack));
							$this->log($token, 'Pop stack and add into the output.', $this->output, $this->stack);
						}
					}

					// If the stack runs out without finding a left parenthesis, then there are mismatched parentheses.
					if (!$leftBracket)
						throw new \InvalidArgumentException('Found the right bracket, but cannot find the left bracket.');

					// Pop the left parenthesis from the temporary stack, but not onto the output stack.
					array_pop($this->stack);
					$this->log($token, 'Pop the stack.', $this->output, $this->stack);
				}
				// If the token is a value, push it onto the stack.
				else
				{
					array_push($this->output, $token);
					$this->log($token, 'Adding token to the the output.', $this->output, $this->stack);
				}
			}
		}

		// When there are no more tokens to read...
		if (!empty($this->stack))
		{
			// ... and while there are still operator tokens in the stack...
			while (end($this->stack))
			{
				// ... pop the operator onto the output stack.
				array_push($this->output, array_pop($this->stack));
				$this->log($token, 'Pop stack and add into the output.', $this->output, $this->stack);
			}
		}

		if($this->loggable)
			$this->printLog();

		// Nice.
	}

	/**
	 * Set the log argument.
	 *
	 * @param bool|true $log
	 */
	public function loggable($log = true)
	{
		$this->loggable = (bool) $log;
	}

	/**
	 * Return postfixed output.
	 *
	 * @return array
	 */
	public function get()
	{
		return $this->output;
	}

	/**
	 * Add action to the log.
	 *
	 * @param $token
	 * @param $action
	 * @param $output
	 * @param $stack
	 * @param string $note
	 */
	protected function log($token, $action, $output, $stack, $note = '')
	{
		array_push($this->log, [
			'token'  => $token,
			'action' => $action,
			'output' => $output,
			'stack'  => $stack,
			'note'   => $note
		]);
	}

	/**
	 * Funky table printing.
	 */
	protected function printLog()
	{
		echo '<table style="width: 100%" border="1">';
		echo '<thead><tr><td>Token</td><td>Action</td><td>Output</td><td>Stack</td><td>Note</td></tr></thead>';

		echo '<tbody>';
		foreach ($this->log as $item)
		{
			echo '<tr>';
			echo '<td>' . $item['token']->getValue() . '</td>';
			echo '<td>' . $item['action'] . '</td>';

			echo '<td>';
			foreach ($item['output'] as $output)
			{
				echo $output->getValue() . ' ';
			}
			echo '</td>';

			echo '<td>';
			foreach ($item['stack'] as $stack)
			{
				echo $stack->getValue() . ' ';
			}
			echo '</td>';

			echo '<td>' . $item['note'] . '</td>';
			echo '</tr>';
		}
		echo '</tbody>';
		echo '</table>';
		echo '<hr>';
	}
}