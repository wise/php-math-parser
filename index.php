<?php

require_once 'vendor/autoload.php';

$parser = new \Math\Parser();

echo $parser->evaluate('6 * ((26 / 2) + -3) - 20'); // 40
echo $parser->evaluate('1 * (2 + 3 / 4)'); // 2.75
echo $parser->evaluate('3 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3'); // 3.0001220703125